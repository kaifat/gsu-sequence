<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>Sequence generator</title>


<script type="text/javascript"
	src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {


        var data = google.visualization.arrayToDataTable([
          ['Number', 'Value'],
           <c:forEach var="elem" items="${sequence}">
                ['', ${elem}], 
            </c:forEach>]); 
        var options = {
        	    title: 'Uniform Distribution',
        	    legend: { position: 'none' },
        	    colors: ['#4285F4'],

        	    chartArea: { width: 401 },
        	    hAxis: {
        	      ticks: [1, 1.5 , 2, 2.5, 3, 3.5, 4, 4.5 , 5, 5.5, 6]
        	    },
        	    bar: { gap: 0 },

        	    histogram: {
        	      bucketSize: 0.5,
        	      maxNumBuckets: 200,
        	      minValue: 1,
        	      maxValue: 6
        	    }
         	 };

                var chart = new google.visualization.Histogram(document.getElementById('chart_div'));
                chart.draw(data, options);
              }
</script>

</head>
<body>

	<form method="POST" action="controller">
		<input type="hidden" name="command" value="generate" /> <input
			type="submit" value="generate" />
	</form>
	<br/> avg = ${ avg }
	<br/> dispersion = ${ dispersion }
	<br/>

	<div id="chart_div" style="width: 900px; height: 500px;"></div>
	
	<br/>
	<c:forEach var="number" items="${sequence}" varStatus="status">
		<br/>
		${number}
	</c:forEach>
	
</body>
</html>