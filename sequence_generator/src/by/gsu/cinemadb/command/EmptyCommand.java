package by.gsu.cinemadb.command;

import javax.servlet.http.HttpServletRequest;

import by.gsu.cinemadb.resource.ConfigurationManager;
public class EmptyCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		/* In case of error or direct access to the controller,
		 * forwarding to the index page */
		String page = ConfigurationManager.getProperty("path.page.index");
		return page;
	}
}