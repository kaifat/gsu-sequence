package by.gsu.cinemadb.command;

import javax.servlet.http.HttpServletRequest;

import by.gsu.cinemadb.model.Sequence;
import by.gsu.cinemadb.resource.ConfigurationManager;

public class GenerateCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		Sequence seq = new Sequence();
		seq.generate();
		seq.sort();
		request.setAttribute("sequence", seq.getSeq());
		request.setAttribute("avg", seq.getAvg());
		request.setAttribute("dispersion", seq.getDispersion());
		page = ConfigurationManager.getProperty("path.page.index");
		return page;
	}

}
