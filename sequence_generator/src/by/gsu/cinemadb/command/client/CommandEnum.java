package by.gsu.cinemadb.command.client;

import by.gsu.cinemadb.command.ActionCommand;
import by.gsu.cinemadb.command.GenerateCommand;;

public enum CommandEnum {

	GENERATE {
		{
			this.command = new GenerateCommand();
		}
	};

			
	ActionCommand command;
	public ActionCommand getCurrentCommand() {
		return command;
	}
}
