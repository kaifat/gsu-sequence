package by.gsu.cinemadb.command.factory;
import javax.servlet.http.HttpServletRequest;

import by.gsu.cinemadb.command.ActionCommand;
import by.gsu.cinemadb.command.EmptyCommand;
import by.gsu.cinemadb.command.client.CommandEnum;
import by.gsu.cinemadb.resource.MessageManager;

public class ActionFactory {
	public ActionCommand defineCommand(HttpServletRequest request) {
		ActionCommand current = new EmptyCommand();
		// extracting the command name from the request
		String action = request.getParameter("command");
		if (action == null || action.isEmpty()) {
			// if command is not specified in the current request
			return current;
		}
		// Receiving an object corresponding to the command
		try {
			CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
			current = currentEnum.getCurrentCommand();
		} catch (IllegalArgumentException e) {
			request.setAttribute("wrongAction", action
					+ MessageManager.getProperty("message.wrongaction"));
		}
		return current;
	}
}
