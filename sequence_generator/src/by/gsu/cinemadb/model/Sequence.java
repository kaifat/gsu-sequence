package by.gsu.cinemadb.model;

import java.util.Arrays;
import java.util.Random;

public class Sequence {
	private static final int LENGTH = 50;
	private static final int MIN_VALUE = 1;
	private static final int MAX_VALUE = 6;
	private float avg;
	private float dispersion;
	private float[] seq;
	
	public Sequence() {
	}

	public static int getLength() {
		return LENGTH;
	}

	public static int getMinValue() {
		return MIN_VALUE;
	}

	public static int getMaxValue() {
		return MAX_VALUE;
	}

	public float getAvg() {
		return avg;
	}

	public float getDispersion() {
		return dispersion;
	}

	public float[] getSeq() {
		return seq;
	}

	public void generate() {
		Random random = new Random();
		float sum = 0;
		this.seq = new float[LENGTH + LENGTH];
		for (int i = 0; i < (LENGTH); i++) {
			this.seq[i] = random.nextFloat() + random.nextInt(MAX_VALUE-1) + MIN_VALUE;
			sum += this.seq[i];
		}
		for (int i = LENGTH - 1; i < (LENGTH + LENGTH); i++) {
			this.seq[i] = random.nextFloat() + random.nextInt(MAX_VALUE-1) + MIN_VALUE;
			sum += this.seq[i];
		}
		this.avg = sum / (LENGTH + LENGTH);
		for (int i = 0; i < (LENGTH + LENGTH); i++) {
			this.dispersion += (this.seq[i] - this.avg) * (this.seq[i] - this.avg) / (LENGTH + LENGTH);
		}
		
	}
	
	public void sort() {
		Arrays.sort(this.seq);
	}

}
