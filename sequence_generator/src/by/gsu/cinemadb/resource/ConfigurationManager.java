package by.gsu.cinemadb.resource;

import java.util.ResourceBundle;
public class ConfigurationManager {
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.config");
	// The class extracts information from the config.properties file
	private ConfigurationManager() { }
	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
